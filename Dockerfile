FROM debian:bullseye
RUN apt-get update  && apt-get install -y nginx
COPY --chown=www-data:www-data ./web_1 /var/www/html
WORKDIR /var/www/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]